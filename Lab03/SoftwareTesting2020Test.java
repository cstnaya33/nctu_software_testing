import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.matchers.Null;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.rmi.StubNotFoundException;
import java.sql.Struct;
import java.util.ArrayList;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
class SoftwareTesting2020Test {
    @Test
    public void test_a() {
        Student std1 = mock(Student.class);
        when(std1.getTemperature()).thenReturn((double)38); //fever

        SoftwareTesting2020 t1 =  spy(SoftwareTesting2020.class);
        t1.date = mock(Date.class);
        t1.hospital = mock(Hospital.class);

        when(t1.date.getWeekday()).thenReturn(4); //Wed

        String s = "test";
        try {
            s = t1.enterClass(std1);
        } catch (InterruptedException e) {
            if (e != null) {
                e.printStackTrace();
            }
        }
        System.out.println(s);
        verify(t1.hospital, never()).treatment(std1);
    }

    @Test
    public void test_b() {
        String treat = "After a long time treatment. The student can get out! ^__^";

        Student std2 = mock(Student.class);
        when(std2.getTemperature()).thenReturn((double)38);

        SoftwareTesting2020 t2 =  spy(SoftwareTesting2020.class);
        t2.date = mock(Date.class);
        t2.hospital = mock(Hospital.class);

        when(t2.date.getWeekday()).thenReturn(5); //Thurs.
        when(t2.hospital.treatment(std2)).thenReturn(treat);

        String s = "test";
        try {
            s = t2.enterClass(std2);
        } catch (InterruptedException e) {
            if (e != null) {
                e.printStackTrace();
            }

        }
        System.out.println(s);
    }


    @Test
    public void test_c() throws InterruptedException {
        //spy
        Student s1 = spy(Student.class);
        Student s2 = spy(Student.class);
        Student s3 = spy(Student.class);

        when(s1.getStudentId()).thenReturn("111");
        when(s2.getStudentId()).thenReturn("222");
        when(s3.getStudentId()).thenReturn("333");

        SoftwareTesting2020 t1 = spy(SoftwareTesting2020.class);
        t1.hospital = spy(Hospital.class);

        doThrow(new IllegalStateException("Error occurred")).when(t1.hospital).isolation(s1);
        doThrow(new IllegalStateException("Error occurred")).when(t1.hospital).isolation(s2);
        doThrow(new IllegalStateException("Error occurred")).when(t1.hospital).isolation(s3);


        try {
            System.out.println( t1.enterClass(s1) );
            System.out.println( t1.enterClass(s2) );
            System.out.println( t1.enterClass(s3) );
        } catch (InterruptedException e) {  }

        ArrayList arr = t1.hospital.getLog();
        arr.forEach(System.out::println);

    }

    @Test
    public void test_d () {
        Student s1 = spy(Student.class);

        //Fake method:
        SoftwareTesting2020 t1 = spy(SoftwareTesting2020.class);
        t1.MyDatabase = new NCTUDatabase() {
            @Override
            public int getScore(String studentid) {
                return 100;
            }
        };
        //System.out.println( t1.MyDatabase.getScore(s1.getStudentId()) );

        //Stub method
        class StubDB implements NCTUDatabase{
            public int getScore(String studentid){
                return 100;
            }
        }

        StubDB s = new StubDB();
        System.out.println(s.getScore(s1.getStudentId()));

    }

    @Test
    public void test_e(){
        SoftwareTesting2020 t1 = spy(SoftwareTesting2020.class);
        paypalService fakeP = new paypalService() {
            @Override
            public String doDonate() {
                return "successed";
            }
        };

        String s = t1.donate(fakeP);
        System.out.println(s);
    }
}
