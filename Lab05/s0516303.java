package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class s0516303 {
    public static void main(String[] args) throws InterruptedException {
        //setting driver
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        //開啟瀏覽器到 NCTU 首頁
        driver.get("https://www.nctu.edu.tw/");
        driver.manage().window().maximize();

        //news
        driver.findElement(By.xpath("/html/body/div[2]/nav/div[1]/div[1]/ul/li[2]/a")).click();

        //sec. title
        driver.findElement(By.xpath("//*[@id=\"modRandomArticle208\"]/div[2]/div/h4/a")).click();
        String title = "";
        title = driver.findElement(By.id("ar-header")).getText();
        System.out.println(title);
        String content = "";
        content = driver.findElement(By.xpath("//*[@id=\"k2Container\"]/div[2]/div[2]")).getText();
        System.out.println(content);

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

        driver.get("https://www.google.com.tw/");

        //search col.
        WebElement search = driver.findElement(By.name("q"));
        search.sendKeys("0516303\n");

        Thread.sleep(1000*2);
        //result = driver.findElement(By.linkText("YC0516303")).getText();
        WebElement result = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//a/h3")));
        System.out.println(result.getText());

        driver.quit();
    }
}
