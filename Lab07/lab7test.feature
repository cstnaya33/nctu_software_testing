Feature: Division
  Do the division.

  Scenario Outline: Divider
    Given There are two floats: <f1> and <f2>
    When  I ask what is the Quotient
    Then  I should be told <f3>
    Examples:
      | f1  | f2  | f3   |
      | 4.3 | 5.5 | 0.78 |
      | 8.7 | 4.6 | 1.89 |

  Scenario Outline: If divisor is zero
    Given two numbers <i1> and <i2>
    When  divisor is equal to zero
    Then  Throw error message error
    Examples:
      | i1  | i2 |
      | 1.0 | 0  |
