package com.example;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import java.text.DecimalFormat;

public class Stepdefs {
    /*
    String isItFriday(String today){
        return "Nope";
    }
    String today;
    String actualAnswer;

    @Given("today is Sunday")
    public void todayIsYearMonthDay() {
        //throw new io.cucumber.java.PendingException();
        today = "Sunday";
    }

    @When("I ask whether it's Friday yet")
    public void iAskWhetherItSFridayYet() {
        //throw new io.cucumber.java.PendingException();
        actualAnswer = isItFriday(today);
    }

    @Then("I should be told {string}")
    public void iShouldBeTold(String expectedAnswer) {
        //throw new io.cucumber.java.PendingException();
        //assertEquals(expectedAnswer, actualAnswer);
    }
    */


    double Division(double d1, double d2) {
        String d = new DecimalFormat("0.00").format(d1/d2);
        System.out.println(d);
        double dd = Double.parseDouble(d);
        return dd;
    }
    double d1, d2 ;
    double Q1 ;
    /*
    @Given("There are two numbers: <d{int}> and <d{int}>")
    public void thereAreTwoNumbersDAndD(double arg0, double arg1) {
        d1 = arg0;
        d2 = arg1;
    }*/
    @Given("There are two floats: {double} and {double}")
    public void there_are_two_floats_and(Double double1, Double double2) {
        d1 = double1;
        d2 = double2;
    }

    @When("I ask what is the Quotient")
    public void iAskWhatIsTheQuotient() {
        Q1 = Division(d1, d2);
    }
    @Then("I should be told {double}")
    public void iShouldBeToldD(Double ExpectedAnswer) {
        assertEquals(Q1, ExpectedAnswer);
        System.out.println(d1 + " divided " + d2 + " = " + Q1 + ", same as the expected one.");
    }

    double  i1;
    int     i2;
    boolean Correct = true;


    @Given("two numbers {double} and {int}")
    public void two_numbers_and(Double double1, Integer int1) {
        i1 = double1;
        i2 = int1;
    }

    @When("divisor is equal to zero")
    public void divisorIsEqualToZero() {
        if (i2 == 0) {
            Correct = false;
        }
        else {}
    }

    @Then("Throw error message error")
    public void throwErrorMessage() {
        if (Correct == false) {
            throw new cucumber.api.PendingException("Error, divisor is 0.");
        }
    }
}
