import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class VehicleTest {
    Vehicle v1 = new Vehicle();
    Vehicle v2 = new Vehicle(10, "south");

    int newSpeed = 100;
    String newDir = "east";

    @BeforeEach
    void setUp() {
        //System.out.println("It runs before each function.");
        assertEquals(2, Calculator.add(1, 1), "1 + 1 should equal 2.");
    }

    @AfterEach
    void tearDown() {
        //System.out.println("It runs after each function.");
        assertEquals(2, Calculator.add(1, 1), "1 + 1 should equal 2.");
    }

    @Test
    void testFinalize() {

        v1 = null;
        v2 = null;
        System.gc();

    }


    @Test
    void setSpeedTest() {
        v1.setSpeed(newSpeed);
        assertEquals(newSpeed, v1.getSpeed());
    }

    @Test
    void setDir() {
        v1.setDir(newDir);
        assertEquals(newDir, v1.getDir());
    }

    @Test
    void getSpeed() {
        assertEquals(10, v2.getSpeed());
    }

    @Test
    void getDir() {
        assertEquals("south", v2.getDir());
    }

    @Test
    void totalVehicle() {
        assertEquals(2, Vehicle.totalVehicle());
    }
}
